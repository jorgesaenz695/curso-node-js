const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
  name: { type: String, require: true },
  lastName: { type: String },
  email: { type: String, require: true, unique: true, index: true },
  password: { type: String, require: true },
  avatar: { type : String, default: 'default.png' }
});

module.exports = mongoose.model('users', usersSchema);