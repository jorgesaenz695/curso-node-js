const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const purchaseOrdersSchema = new Schema({
  nameClient: { type: String, require: true },
  tipDoc: { type: String, require: true },
  numDoc: { type: String, require: true },
  addrClient: { type: String, require: true },
  phoneClient: { type: String, require: true },
  emisionDate : {type: Date, require: true},
  listProducts: {type: Array, require: true}
});

module.exports = mongoose.model('purchaseOrders', purchaseOrdersSchema);