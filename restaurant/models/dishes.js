const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dishesSchema = new Schema({
  idCategory: { type: String, require: true },
  name: { type: String, require: true },
  description: { type: String },
  price: { type: String, require: true},
  date: { type: Date, require: true }
});

module.exports = mongoose.model('dishes', dishesSchema);