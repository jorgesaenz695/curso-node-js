var express = require('express');
var router = express.Router();
const dishesController = require('../controllers/dishes');

//List all dishes
router.get('/', (req,res)=>{
    dishesController.list().then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Dishes not found'});
        }
    )
});

//Show info
router.get('/:dish_id/show', (req,res)=>{
    const idDish = req.params.dish_id;
    dishesController.show(idDish).then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Dish not exists'});  
        }
    )
});

//Create Dish
router.post('/',(req,res)=>{
    const {idCategory, name, description, price, date} = req.body;
    dishesController.store(idCategory, name, description, price, date).then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err);
        }
    )
});

//Update Dish
router.put('/:dish_id', (req,res) => {
    const {idCategory, name, description, price, date} = req.body;
    const { dish_id } = req.params;

    dishesController.update(idCategory, name, description, price, date, dish_id)
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err);  
        }
    )
});

// Delete dish
router.delete('/:dish_id', (req, res) => {
    const { dish_id } = req.params;

    dishesController.destroy(dish_id)
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err);
        }
    )
  });

module.exports = router;