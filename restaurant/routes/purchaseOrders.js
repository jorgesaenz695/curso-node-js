var express = require('express');
var router = express.Router();
const purchaseOrdersController = require('../controllers/purchaseOrders');

//List all purchase orders
router.get('/', (req,res)=>{
    purchaseOrdersController.list()
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Purchase orders not found'});
        }
    )
});

//Show info
router.get('/:purchaseOrders_id/show', (req,res)=>{
    const idPurchaseOrder = req.params.purchaseOrders_id;

    purchaseOrdersController.show(idPurchaseOrder)
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Purchase orders not exists'});
        }
    )
});

//Create purchase orden
router.post('/',(req,res)=>{
    const { 
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts
    } = req.body;

    purchaseOrdersController.store(
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts).then(
            (result) => {
                res.json(result);
            },
            (err) => {
                res.status(500).json(err);
            }
    )
});

//Update purchase orders
router.put('/:purchaseOrder_id', (req,res) => {
    const { 
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts
    } = req.body;

    const { purchaseOrder_id } = req.params;

    purchaseOrdersController.update(
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts,
        purchaseOrder_id)
        .then(
            (result) => {
                res.json(result);
            },
            (err) => {
                res.status(500).json(err);
            }
        )
});

module.exports = router;