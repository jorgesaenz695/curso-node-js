var express = require('express');
var router = express.Router();
const categoriesControllers = require('../controllers/categories');

//List all categories
router.get('/', (req,res)=>{
    categoriesControllers.list().then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Categories not exists'});
        }
    )
});

//Show info
router.get('/:category_id/show', (req,res)=>{
    const idCategory= req.params.category_id;
    categoriesControllers.show(idCategory).then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json({message : 'Categorie not exists'});
        }
    )    
});

//Create Category
router.post('/',(req,res)=>{
    const {name, description} = req.body;
    categoriesControllers.store(name, description).then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err);
        }
    )
});

//Update Category
router.put('/:category_id', (req,res) => {
    const { name, description} = req.body;
    const { category_id } = req.params;

    categoriesControllers.update(name,description,category_id)
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err);
        }
    )
});

//Delete Category
router.delete('/:category_id', (req, res) => {
    const { category_id } = req.params;

    categoriesControllers.destroy(category_id)
    .then(
        (result) => {
            res.json(result);
        },
        (err) => {
            res.status(500).json(err); 
        }
    )
  });

module.exports = router;