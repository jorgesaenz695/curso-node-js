const express = require('express');
const router = express.Router();
const usersControllers = require('../controllers/users');

// Listar
router.get('/', function(req, res, next) {
  usersControllers.list().then(
    (result) => {
      res.json(result);
    },
    (err) => {
      res.status(500).json({ message: 'Ocurrio un error' });
    }
  )
});

// Mostrar info
router.get('/:user_id/show', (req, res) => {
  const userId = req.params.user_id;
  usersControllers.show(userId)
  .then(
    (result) => {
      res.json(result);
    },
    (err) => {
      res.status(500).json(err);
    }
  ).catch( (ex) =>{ 
    res.status(500).json(ex);
  })
});

// Crear
router.post('/', (req, res) => {
  const { name, lastName, email, password, avatar} = req.body;
  usersControllers.store( name, lastName, email, password, avatar)
  .then(
    (result) => {
      res.json(result);
    },
    (err) => {
      res.status(500).json(err);
    }
  );
});

// Update 
router.put('/:user_id', (req, res) => {
  const { name, lastName, email, password, avatar} = req.body;
  const { user_id } = req.params;
  usersControllers.update( user_id, name, lastName, email, password, avatar)
  .then(
    (result) => {
      res.json(result);
    },
    (err) => {
      res.status(500).json(err);
    }
  )
});

// Delete ?id=12
router.delete('/:user_id', (req, res) => {
  const { user_id } = req.params;
  usersControllers.destroy( user_id )
  .then(
    (result) => { res.json(result) },
    (err) => {
      res.status(500).json(err);
    }
  )
});



module.exports = router;
