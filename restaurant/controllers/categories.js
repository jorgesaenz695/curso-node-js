const _ = require('lodash');
const categoriesModel = require('../models/categories');

class Categories {
  // Listar
  list() {
    return new Promise( (resolve, reject) => {
      // Promise
      categoriesModel.find({}, {_id: 1, name: 1, description: 1}).then(
        (result) => {
          return resolve( result );
        },
        (err) => {
          return reject( err );
        }
      );
      
    });
  }

  // Mostrar info
  show(idCategory){
    return new Promise ((resolve, reject) => {

      categoriesModel.findOne(
        {_id:idCategory}, {_id:1, name: 1, description: 1}
      ).then(
        (result) => {
          return resolve (result);
        },
        (err) => {
          return reject(err);
        }
      );

    });
  }

  //Create Category
  store(name, description) {
    return new Promise((resolve, reject) => {

        categoriesModel.create({name, description})
        .then(
          (category) => {
            return resolve({
              _id: category._id,
              name: name,
              description: description,
            });
          },
          (err)=>{
            return reject(err);
          }
        );

    });
  }

  //Update Category
  update(name, description, category_id) {
    return new Promise((resolve, reject) => {
      let newCategory = {
        name,
        description
      }

      categoriesModel.update(
        { _id: category_id },
        { $set: newCategory }
      )
      .then(
        (result) => {
          return resolve(
            {
              _id: category_id,
              name,
              description
            }
          )
        },
        (err) => {
          return reject(err);
        }
      );

    });
  }

  //Delete Category
  destroy(category_id) {
    return new Promise((resolve,reject)=>{

      categoriesModel.remove({_id: category_id})
      .then(
        (result)=>{
          return resolve({success: true});
        },
        (err) => {
          return reject(err);
        }
      );

    });
  }

}

module.exports = new Categories;
