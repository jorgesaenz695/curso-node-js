const _ = require('lodash');
const usersModel = require('../models/users');

const listUsers = [
  { id: 1, name:'Kelvin', lastName: 'Zevallos' },
  { id: 2, name:'Kelvin', lastName: 'Torre blanca' }
];

class Users {
  // Listar
  list() {
    return new Promise( (resolve, reject) => {
      // Promise
      usersModel.find({}, {_id: 1, name: 1, avatar: 1}).then(
        (result) => {
          return resolve( result );
        },
        (err) => {
          return reject( err );
        }
      );
    });
  }

  // Mostrar info
  show(userId) {
    return new Promise( (resolve, reject) => {
      usersModel.findOne(
        { _id: userId }, 
        { _id: 1, name: 1, avatar: 1 }
      ).then(
        (result) => {
          return resolve(result);
        },
        (err) => {
          return reject(err);
        }
      )
    });
  }

  store( name, lastName, email, password, avatar) {
    return new Promise( (resolve, reject) => {
      usersModel.create(
        {
          name,
          lastName,
          email,
          password,
          avatar,
        }
      ).then(
        (user) => {
          return resolve({ 
            _id: user._id,
            name: user.name,
            lastName: user.lastName
          });
        },
        (err) => {
          return reject(err);
        }
      );
    });
  }

  update(userId, name, lastName, email, password, avatar) {
    return new Promise((resolve, reject) => {
      let newUser = {
        name,
        lastName,
        email,
        avatar
      };
      if (password) {
        // si existe
        newUser =  { ...newUser, password }
      }

      usersModel.update(
        { _id: userId },
        { $set : newUser }
      ).then(
        (result)=>{
          return resolve({
            _id: userId,
            name,
            lastName
          });
        },
        (err) => {
          return reject(err)
        }
      )
    });
  }

  destroy(userId) {
    return new Promise((resolve, reject) => {
      usersModel.remove(
        { _id: userId }
      ).then(
        (result) => {
          return resolve({ success: true });
        },
        (err ) => {
          return reject(err)
        }
      );
    });
  }

}

module.exports = new Users;