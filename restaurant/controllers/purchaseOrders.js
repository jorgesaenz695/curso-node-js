const _ = require('lodash');
const purchaseOrdersModel = require('../models/purchaseOrders');

class PurchaseOrders {
  // Listar purchase orders
  list() {
    return new Promise( (resolve, reject) => {
      // Promise
      purchaseOrdersModel.find({},{
        _id:1,
        nameClient:1,
        tipDoc:1,
        numDoc:1,
        addrClient:1,
        phoneClient:1,
        emisionDate:1,
        listProducts:1
      }).then(
        (result) => {
          return resolve(result);
        },
        (err) => {
          return reject(err);
        }
      );
    });
  }

  // Mostrar info purchase orders
  show(idPurchaseOrder){
    return new Promise((resolve, reject) => {

      purchaseOrdersModel.findOne({_id: idPurchaseOrder},
        {
          _id:1,
          nameClient:1,
          tipDoc:1,
          numDoc:1,
          addrClient:1,
          phoneClient:1,
          emisionDate:1,
          listProducts:1
        }
      ).then(
        (result) => {
          return resolve(result);
        },
        (err) => {
          return reject(err);
        }
      );

    });
  }

  //Create purchase orders
  store(
    nameClient,
    tipDoc, 
    numDoc, 
    addrClient,
    phoneClient,
    emisionDate,
    listProducts) {

    return new Promise((resolve, reject) => {

      const purchaseOrder = {
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts
      }

      purchaseOrdersModel.create(purchaseOrder).then(
        (result) => {

          return resolve(result);
        },
        (err) => {
          return reject(err);
        }
      )
    });
  }

  //Update purchase orders
  update(
    nameClient,
    tipDoc, 
    numDoc, 
    addrClient,
    phoneClient,
    emisionDate,
    listProducts,
    purchaseOrder_id) {

    return new Promise((resolve, reject) => {

      const purchaseOrderUpdate = {
        nameClient,
        tipDoc, 
        numDoc, 
        addrClient,
        phoneClient,
        emisionDate,
        listProducts
      }

      purchaseOrdersModel.update(
        {_id: purchaseOrder_id},
        {$set: purchaseOrderUpdate}
      ).then(
        (result) => {
          return resolve(result);
        },
        (err)=> {
          return reject(err);
        }
      );
    });
  }

}

module.exports = new PurchaseOrders;
