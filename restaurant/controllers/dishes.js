const _ = require('lodash');
const dishesModel = require('../models/dishes');
const categoriesModel = require('../models/categories');

class Dishes {
  // Listar
  list() {
    return new Promise( (resolve, reject) => {

      //Promise
      dishesModel.find({}, 
        {
          _id: 1,
          name: 1, 
          description: 1, 
          price: 1, 
          date:1,
          idCategory:1
        }
        ).then(
        (result) => {
         
          //#region Search info category
          categoriesModel.find({},{_id:1, name: 1, description: 1}).then(
            (category) => {
              let listDishes = result;
              let listCategoDish = [];

              listDishes.forEach(dish => {
                let infoCategory = category.find((category) => category.id === dish.idCategory);
                let categorymodel;
                
                if(infoCategory){
                  categorymodel = {
                    _id: infoCategory.id,
                    name: infoCategory.name,
                    description: infoCategory.description
                  }
                }else {
                  categorymodel = {
                    "message" : "Category not found"
                  }
                }
                
                let categoDish = {
                  _id : dish.id,
                  name : dish.name,
                  description : dish.description,
                  price : dish.price,
                  date: dish.date,
                  category: categorymodel
                }

                listCategoDish.push(categoDish);

              });
    
              return resolve(listCategoDish);

            },
            (err) => {
              return reject(err);
          });
          //#endregion

        },
        (err) => {
          return reject(err);
        }
      );

    //   dishesModel.aggregate([
    //     {
    //       $lookup:
    //         {
    //           from: "categories",
    //           localField: "idCategory.str",
    //           foreignField: "_id.str",
    //           as: "category"
    //         }
    //      },
    //    // define which fields are you want to fetch
    //      {   
    //          $project:{
    //              _id : 1,
    //              name : 1,
    //              description : 1,
    //              price : 1,
    //              date: 1,
    //              category: "$category"
    //          } 
    //      }
    //  ]).then(
    //      (result) => {
    //         return resolve(result);
    //      },
    //      (err) => {
    //         return reject(err);
    //      }
    //    );

    });
  }

  // Mostrar info
  show(idDish){
    return new Promise ((resolve, reject) => {
      dishesModel.findOne(
        { _id: idDish},
        {_id: 1, name: 1, description:1, price: 1, date: 1, idCategory: 1}
      ).then(
        (dish) => {

          let infoDish = {
            _id: dish.id,
            name: dish.name,
            description: dish.description,
            price: dish.price,
            date: dish.date
          }

          //#region find info category
          let infoCategoryModel;

          categoriesModel.findOne({_id: dish.idCategory},{_id:1, name: 1, description: 1}).then(
            (category) => {

              infoCategoryModel = {
                _id: category.id,
                name: category.name,
                description: category.description
              }

              return resolve(
                {
                  ...infoDish,
                  category: infoCategoryModel
                }
              )

            },
            (err) => {
              return reject( 
                {
                  ...infoDish,
                  category: {"message" : "Category not found"}
                }
              )
            }
          );
          //#endregion
        },
        (err)=>{
          return reject(err);
        }
      )
    });
  }

  //Create Dish
  store(idCategory, name, description, price, date) {
    return new Promise((resolve, reject) => {

      //Verify if category exists
      categoriesModel.findOne({_id: idCategory},{_id: 1, name: 1}).then(
        (result) => {
            //#region Create dish
            dishesModel.create({
              idCategory,
              name,
              description,
              price,
              date
            }).then(
            (dish)=> {
              return resolve({
                _id: dish._id,
                name,
                description,
                price,
                date
              });
            },
            (err)=>{
              return reject(err);
            })
            //#endregion
        },
        (err) => {
          return reject(
            {
              "message" : "The category to be associated does not exist"
            }
          );
        }
      );

    });
  }

  //Update dish
  update(idCategory, name, description, price, date, idDish) {
    return new Promise((resolve, reject) => {

      //Verify if category exists
      categoriesModel.findOne({_id: idCategory},{_id: 1, name: 1}).then(
        (result) => {
            //#region update dish
            let newDish = {
              idCategory,
              name,
              description,
              price,
              date
            };
      
            dishesModel.update(
              {_id: idDish},
              { $set: newDish}
            ).then(
              (result) => {
                return resolve(result);
              },
              (err)=>{
                return reject(err);
              }
            )
            //#endregion
        },
        (err) => {
          return reject(
            {
              "message" : "The category to be associated does not exist"
            }
          );
        }
      );

    });
  }

  //Delete dish
  destroy(dish_id) {
    return new Promise((resolve,reject)=>{
      dishesModel.remove({ _id: dish_id})
      .then(
        (result) => {
          return resolve({success : true});
        },
        (err)=>{
          return reject(err);
        }
      );
    });
  }

}

module.exports = new Dishes;
